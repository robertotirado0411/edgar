/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agencia;

/**
 *
 * @author Edgar Guerrero
 */
public class Agencia {
    private int numBoleto;
    private String destino;
    private String nombreCliente;
    private int años;
    private int tipoViaje;
    private float precio;
    
    public Agencia(){
        
    }
    
    
    public Agencia(int numBoleto, String destino, String nombreCliente, int años, int tipoViaje, float precio) {
        this.numBoleto = numBoleto;
        this.destino = destino;
        this.nombreCliente = nombreCliente;
        this.años = años;
        this.tipoViaje = tipoViaje;
        this.precio = precio;
    }
    
    public Agencia(Agencia otro) {
        this.numBoleto = otro.numBoleto;
        this.destino = otro.destino;
        this.nombreCliente = otro.nombreCliente;
        this.años = otro.años;
        this.tipoViaje = otro.tipoViaje;
    }    

    public int getNumBoleto() {
        return numBoleto;
    }

    public void setNumBoleto(int numBoleto) {
        this.numBoleto = numBoleto;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public int getAños() {
        return años;
    }

    public void setAños(int años) {
        this.años = años;
    }

    public int getTipoViaje() {
        return tipoViaje;
    }

    public void setTipoViaje(int tipoViaje) {
        this.tipoViaje = tipoViaje;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    
    
// Función para calcular el subtotal
public float calcularSubtotal() {
    float sub=0.0f;
       if (this.tipoViaje == 1){
           sub = this.precio;
       }
       if (this.tipoViaje == 2){
           sub = (float) (this.precio + this.precio * .8);
       }
    return sub;
}  

// Función para calcular el impuesto
public float calcularImpuesto() {
    float impuesto = 0.0f;
    impuesto = this.precio *.16f;
    return impuesto;
}

// Función para calcular el descuento
public float calcularDescuento() {
    float descuento=0.0f;
    if (this.años >= 60 && this.tipoViaje == 1) {
        descuento = (float) (this.precio * 0.5);
    } else if (this.tipoViaje == 2) {
        descuento = (float) (this.precio * 0.5);
    } else {
        descuento = 0;
    }
 return descuento;
}

// Función para calcular el total a pagar
public float calcularTotalAPagar() {
    float totalPagar=0.0f;
    totalPagar= calcularSubtotal() + calcularImpuesto() - calcularDescuento();
    return totalPagar;
}
    
    
}
